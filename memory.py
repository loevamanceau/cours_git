import numpy as np
import random

"""
@author vcoindet
Jeu de memory
"""

Tabl = ['a', 'a', 'b', 'b', 'c', 'c', 'd', 'd', 'e', 'e', 'f', 'f']

def melange_carte(Tab):
    """
    

    Parameters
    ----------
    Tab : TYPE list
        DESCRIPTION. tableau des cartes rangées dans l'ordre

    Returns
    -------
    Tab : TYPE list
        DESCRIPTION.tableau des cartes mélangées
    
    """
    for i in range(1, len(Tab)):
        # pick an element in Tab[:i+1] with which to exchange Tab[i]
        j = int(random.random() * (i+1))
        Tab[i], Tab[j] = Tab[j], Tab[i]
    return Tab

def carte_cache(Tab):   #TODO
    """
        TODO
        Doit créer une liste de même longueur que la liste renvoyés dans la question précédentes
        T_cache: liste de la même taille que celle de départ
        composée de * à la place des cartes
    """
    T_cache = [0 for i in range(0,len(Tab))]
        
    return T_cache


def choisir_cartes(Tab):
    """
    

    Parameters
    ----------
    Tab : TYPE list
        DESCRIPTION.

    Returns
    -------
    list 
        DESCRIPTION. liste de 2 cartes différentes

    """
    c1 = int(input("Choisissez une carte : "))
    print(Tab[c1])
    c2 = int(input("Choisissez une deuxieme carte : "))
    while c1 == c2:
        print("Erreur, la deuxieme carte ne peut être la même que la premiere ! ")
        c2 = int(int("Veuillez choisir une deuxieme carte différente de la premiere : "))
    print(Tab[c2])
    return [c1,c2]


def retourne_carte (c1, c2, Tab, Tab_cache):    #TODO
    """
        TODO
        doit retrouner les cartes dans la liste cachée
    """
    new_tab = Tab_cache
    new_tab[c1] = Tab[c1]
    new_tab[c2] = Tab[c2]
    
    return new_tab
    
    
    

def jouer(Tab):
    """
    jouer un memory

    Parameters
    ----------
    Tab : TYPE liste
        DESCRIPTION: contient les paires pour jouer

    Returns "Bravo, tu as gagné"
    -------
    None.

    """
    print("Bienvenue sur notre Jeu Memory, ci dessous voici les cartes mise en jeu. Il y en a 12")
    print(Tab)
    Tab = melange_carte(Tab)
    print("Maintenant je vais melanger les cartes et les retourner")
    
    Tab_cache =  carte_cache(Tab)
    print(Tab_cache)

    [c1, c2] = choisir_cartes(Tab)
    Tab_cache = retourne_carte(c1, c2, Tab, Tab_cache)
    print(Tab_cache)
    
    while 0 in Tab_cache:
        [c1, c2] = choisir_cartes(Tab)
        
        Tab_cache = retourne_carte(c1, c2, Tab, Tab_cache)
        
        print(Tab_cache)
        
    print("Bravo, tu as gagné!!!")

if __name__ == "main":    
    jouer(Tabl)
    
    l = [1,4,6,7]
    nv_l = melange_carte(l)
    print(nv_l)
    
    #Test choisir carte
    c = choisir_cartes(l)
    print(c)
    
    #test carte_cache
    l_cache = carte_cache(l)
    print(l_cache)